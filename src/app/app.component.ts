import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController, Tab } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import * as firebase from 'firebase';

import { CONFIG_FIREBASE } from '../config/firebase.config';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  isAuthenticated: boolean;

  @ViewChild('content') content: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menuCtrl: MenuController) {
    platform.ready().then(() => {
      firebase.initializeApp(CONFIG_FIREBASE);
      this.listenToAuthStateChange();

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  navigateToSettings() {
    this.navigateToPage('SettingsPage');
  }

  navigateToDocuments() {
    this.navigateToPage(TabsPage);
  }

  navigateToPage(page) {
    this.content.setRoot(page);
    this.menuCtrl.close();
  }

  onClickLogout() {
    firebase.auth().signOut();
    this.navigateToPage(TabsPage);
  }

  private listenToAuthStateChange() {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.isAuthenticated = true;
          this.rootPage = TabsPage;
        }
        else {
          this.isAuthenticated = false;
          this.rootPage = LoginPage;
        }
      }
    )
  }
}
