import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { CD } from '../../models/cd';

@IonicPage()
@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html',
})
export class LendCdPage {

  cd: CD;
  nomEmprunteur: string = null;

  constructor(public navParams: NavParams, private viewCtrl: ViewController) {
    this.cd = this.navParams.data.cd;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  emprunter() {
    this.cd.emprunter(this.nomEmprunteur);
    this.nomEmprunteur = null;
  }

}
