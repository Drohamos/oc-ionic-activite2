import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  userForm: FormGroup;
  errorMessage = null;
  showSpinner = false;

  constructor(private authSrv: AuthService, private formBuilder: FormBuilder, private navCtrl: NavController) {
    this.initForm();
  }

  private initForm() {
    this.userForm = this.formBuilder.group({
      // @todo: Enlever valeurs préremplies
      email: ["barkasrobin@gmail.com", [Validators.required, Validators.email]],
      password: ["123456", [Validators.required]]
    });
  }

  private onSubmitForm() {
    this.errorMessage = null;
    this.showSpinner = true;

    const email = this.userForm.get('email').value;
    const password = this.userForm.get('password').value;

    this.authSrv.signInUser(email, password).then(
      () => {
        this.navCtrl.setRoot(TabsPage);
      },
      (error) => {
        console.log(error);
        this.errorMessage = error.message;
        this.showSpinner = false;
      },
    );
  }

  private onClickRegister() {
    this.navCtrl.push('RegisterPage');
  }

}
