import { Component } from '@angular/core';
import { IonicPage, ModalController } from 'ionic-angular';
import { CD } from '../../models/cd';
import { DocumentsService } from '../../services/documents.service';

@IonicPage()
@Component({
  selector: 'page-cd-list',
  templateUrl: 'cd-list.html',
})
export class CdListPage {

  cds: Array<CD>;

  constructor(private documentsSrv: DocumentsService, private modalCtrl: ModalController) {
    this.cds = this.documentsSrv.cds;
  }

  cdSelected(cd: CD) {
    let modal = this.modalCtrl.create('LendCdPage', { cd: cd });
    modal.present();
  }

}
