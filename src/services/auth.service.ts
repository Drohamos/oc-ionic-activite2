import { Injectable } from '@angular/core';
import firebase from 'firebase';

@Injectable()
export class AuthService {
  isAuthenticated = false;

  constructor() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.isAuthenticated = true;
      }
      else {
        this.isAuthenticated = false;
      }
    })
  }

  signUpUser(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(email, password).then(
        (user) => {
          resolve(user);
        },
        (error) => {
          reject(error);
        }
      )
    });
  }

  signInUser(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password).then(
        (user) => {
          resolve(user);
        },
        (error) => {
          reject(error);
        }
      );
    })
  }

  signOut() {
    firebase.auth().signOut();
  }

}
