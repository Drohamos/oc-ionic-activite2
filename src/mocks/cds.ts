import { CD } from "../models/cd";

export const mock_cds: CD[] = [
    new CD("J'en parlerai au diable", "Johnny Hallyday"),
    new CD("Shallow", "Lady Gaga"),
    new CD("Pardonne-moi", "Johnny Hallyday"),
    new CD("A nos souvenirs", "Trois Cafés Gourmands"),
    new CD("Promises", "Calvin Harris", "Félix"),
    new CD("Ramenez la coupe à la maison", "Vegedream"),
    new CD("I'll Never Love Again", "Lady Gaga", "Pierre"),
    new CD("Jaloux", "Dadju")
]