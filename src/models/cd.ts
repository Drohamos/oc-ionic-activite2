import { DocumentBibliotheque } from "./document-bibliotheque";

export class CD extends DocumentBibliotheque {
    artiste: string;

    constructor(titre, artiste, emprunteur?) {
        super(titre, emprunteur);

        this.artiste = artiste;
    }
}