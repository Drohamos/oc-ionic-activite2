export class DocumentBibliotheque {
    titre: string;
    estDisponible: boolean = true;
    emprunteur: string = null;

    constructor(titre, emprunteur?) {
        this.titre = titre;

        if (emprunteur) {
            this.emprunter(emprunteur);
        }
    }

    public emprunter(emprunteur?: string) {
        this.estDisponible = false

        this.emprunteur = ((emprunteur)? emprunteur : "Emprunteur inconnu");
    }

    public rendre() {
        this.estDisponible = true;
        this.emprunteur = null;
    }
}