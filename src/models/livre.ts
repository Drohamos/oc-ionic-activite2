import { DocumentBibliotheque } from "./document-bibliotheque";

export class Livre extends DocumentBibliotheque {
    auteur: string;

    constructor(titre, auteur, emprunteur?) {
        super(titre, emprunteur);

        this.auteur = auteur;
    }
}